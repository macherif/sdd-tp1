// #ifndef TP1.h
// #define TP1.h
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct celll {
	int datedeb;
	int datefin;
	char * msg ;
	struct celll * suiv ;
}cell;

cell * creation(int deb , int fin , char * msg );
cell ** rechprec(cell ** t , int deb ) ;
void insertion (cell * nouv , cell ** t) ;
void ADJ_CELL(cell ** prec , cell * nouv ) ;

// #endif
