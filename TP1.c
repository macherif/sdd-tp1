#include <stdlib.h>

#include "TP1.h"

cell * creation(int deb , int fin , char * msg ){
	
	cell * cellule = (cell *) malloc (sizeof(cell)) ;
	if (cellule) {
		cellule->datedeb = deb;
		cellule->datefin = fin;
		cellule->msg =(char *) malloc (sizeof(char)*strlen(msg));
		if (cellule->msg){
			strcpy(cellule->msg , msg );
		}
		cellule->suiv=NULL;
	}
	return(cellule);
}
cell ** rechprec(cell ** t , int deb ){
	cell * cour = *t;
	cell ** prec = t;
	while ( (cour != NULL ) && (cour->datedeb < deb ) ) {
		cour=cour->suiv;
		prec=&(cour->suiv);
	}
	return(prec);
}
void ADJ_CELL(cell ** prec , cell * nouv ){
	nouv->suiv=*prec;
	*prec=nouv ;
}
void insertion (cell * nouv , cell ** t){
	cell ** prec = rechprec(t,nouv->datedeb );
	ADJ_CELL(prec , nouv );
}

void sauvegarde_fichier(cell ** t){
	cell * cour = *t;
	FILE * fichier = fopen("sauvegarde.txt" , "a");
	if (fichier){
	while (cour  != NULL ){
		fprintf(fichier , "%d %d %s\n",cour->datedeb , cour->datefin , cour->msg );
		cour = cour->suiv;
	}
	fclose(fichier);
}
}
	

int main() {
	
	FILE * fichier = fopen("donnees.txt","r");
	int deb , fin ;
	char message[100] ;
	cell * cellule ;
	cell ** tete = &cellule ;
	if(fichier){
		fscanf(fichier,"%d %d",&deb,&fin);
		while(!feof(fichier)){
			fgets(message,100,fichier);
			message[strlen(message)-1]="\0";
			cellule=creation(deb,fin,message);
			insertion(cellule , tete);
			printf("%d %d %s",cellule->datedeb,cellule->datefin,cellule->msg);
			fscanf(fichier,"%d %d",&deb,&fin);
		}
	}
	sauvegarde_fichier(tete);
	fclose(fichier);
	// printf("%d %d %s",cellule->datedeb,cellule->datefin,cellule->msg);

	return 0;
}
